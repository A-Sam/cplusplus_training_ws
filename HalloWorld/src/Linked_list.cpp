#include <typedef.h>


typedef struct _familyMember{
	char name[20];
	int age;
	char job[20];
	_familyMember* NextFamilyMember;
}familyMember;


void LinkedList_Family () {
	cout << "->Start of LinkedList_Family" << endl;

	int familyMembersTotal;
	cout << "Please Enter the total number of the family members = " << endl;
	cin >> familyMembersTotal;
	familyMember* Member;
	familyMember* temp;
	temp = new familyMember();
	Member = new familyMember();

	for (int i = 0 ; i < familyMembersTotal ; i++) {
		Member = new familyMember();
		Member->NextFamilyMember = temp;
		cout << "Enter the name: "; cin >> Member->name;
		cout << "Enter the age: "; cin >> Member->age;
		cout << "Enter the job: "; cin >> Member->job;
		temp->NextFamilyMember = Member;

		cout << "Name = " <<  Member->name << endl;
		cout << "Age = " <<  Member->age << endl;
		cout << "Job = " <<  Member->job << endl;
		cout << "Next = " <<  (Member->NextFamilyMember)->name << endl;
	}



	cout << "->End of LinkedList_Family" << endl;
}
